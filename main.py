import netron

from model.lstmmodel import LSTM_Model
import pandas as pd

data_directory = './data/'
# file origin = https://www.abcbourse.com/download/historiques.aspx
data_file = "Cotations20180501.csv"
selected_columns = ["Date", "Open", "High", "Low", "Closing", "Volume"]


def main():
    value = ''
    while value != 'q':
        value = input("1-learn, 2-predict, 3-display data, 4-display model (q to quit) :")
        if value == '1':
            lstm = LSTM_Model('lstm_CASA_stock')
            data = lstm.load_data(data_directory, data_file)
            data = pd.DataFrame(data=data, columns=selected_columns)
            data = lstm.convert_date_to_day(data)
            features, labels, ftest, ltest = lstm.get_train_test_data(data, columns_features=['Closing'],
                                                                      test_split=0.25,
                                                                      method='random')
            lstm.build_model(lstm.window_size - 1, 1)
            lstm.compile_model()
            lstm.train_model(features, labels, ftest, ltest, early_stopping=True)
            lstm.evaluate_model(ftest, ltest, 100)
            lstm.display_history(history_type=2)
            lstm.save_model()
        elif value == '2':
            lstm = LSTM_Model('lstm_CASA_stock')
            lstm.build_model(lstm.window_size - 1, 1)
            lstm.compile_model()
            lstm.load_model()
            data_to_predict = lstm.load_data(data_directory, data_file)
            data_to_predict = pd.DataFrame(data=data_to_predict, columns=selected_columns)
            data_to_predict = lstm.convert_date_to_day(data_to_predict)
            lstm.prediction(data_to_predict, columns_features=['Closing'])
            lstm.display_predict()
        elif value == '3':
            lstm = LSTM_Model('lstm_CASA_stock')
            data = lstm.load_data(data_directory, data_file)
            data = pd.DataFrame(data=data, columns=selected_columns)
            data = lstm.convert_date_to_day(data)
            LSTM_Model.display_data(data, 'Closing')
        elif value == '4':
            lstm = LSTM_Model('lstm_CASA_stock')
            lstm.build_model(lstm.window_size - 1, 1)
            lstm.compile_model()
            lstm.load_model()
            lstm.display_model()
        else:
            netron.server.stop()


if __name__ == '__main__':
    main()
