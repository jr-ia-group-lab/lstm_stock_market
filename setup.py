from setuptools import setup

setup(
    name='model',
    version='0.0.1',
    packages=['model'],
    url='',
    license='',
    author='JRG',
    author_email='jrgruser@gmail.com',
    description='Simple LSTM model for stock market prediction',
    install_requires=['Keras', 'netron', 'numpy', 'pandas', 'matplotlib', 'setuptools']
)
