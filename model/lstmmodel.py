import logging
import pickle
import pprint

import keras
import matplotlib.pyplot as plt
import netron
import numpy as np
import pandas as pd
from keras import Sequential, metrics
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Dropout, Dense

from util import Logger, Timer


class LSTM_Model:

    def __init__(self, name):
        self.train_data = None  # Train dataset
        self.logger = Logger('Model LSTM', logging.INFO).get_logger()
        self.name = name
        self.model = Sequential()
        self.loss = 'mean_squared_error'
        self.optimizer = 'Adam'
        self.metrics = [metrics.mae, metrics.mse]
        self.selected_columns = ["Open", "High", "Low", "Closing", "Volume"]
        self.window_size = 5
        self.history = None
        self.predict = None

    def build_model(self, input_timesteps, input_dim):
        timer = Timer()
        timer.start()
        self.window_size = input_timesteps + 1
        self.model.add(LSTM(100, input_shape=(input_timesteps, input_dim), return_sequences=True))
        self.model.add(Dropout(0.2))
        self.model.add(LSTM(100, return_sequences=True))
        self.model.add(LSTM(100, return_sequences=False))
        self.model.add(Dropout(0.2))
        self.model.add(Dense(1, activation='linear', input_dim=input_dim))
        self.logger.info('Model Build')
        timer.stop()
        return self.model

    def compile_model(self):
        timer = Timer()
        timer.start()
        self.model.compile(loss=self.loss,
                           optimizer=self.optimizer,
                           metrics=self.metrics)
        print(self.model.summary())
        self.logger.info('Model Compiled with loss=' + self.loss + ' optimizer=' + self.optimizer)
        timer.stop()
        return self.model

    def train_model(self, x, y, xtest, ytest, epochs=1000, batch_size=32, tensorboard_logging=False,
                    early_stopping=False,
                    monitor='val_loss',
                    min_delta=0, patience=2, verbose=1):
        timer = Timer()
        timer.start()
        self.logger.info('Model ' + self.name + ' Training Started')
        self.logger.info('Model ' + self.name + '[Model] %s epochs, %s batch size' % (epochs, batch_size))
        callbacks = []
        stopped_epoch = epochs
        if tensorboard_logging:
            tensorboard_callback = keras.callbacks.TensorBoard(
                log_dir='tensorboard_' + self.name + '_' + str(epochs))
            callbacks.append(tensorboard_callback)
        if early_stopping:
            self.logger.info('Model ' + self.name + '[EarlyStopping] %s monitor, %s min_delta' % (monitor, min_delta))
            early_stopping_monitor = EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience, verbose=1)
            callbacks.append(early_stopping_monitor)
            stopped_epoch = early_stopping_monitor.stopped_epoch

        history = self.model.fit(x, y,
                                 validation_data=(xtest, ytest),
                                 epochs=epochs,
                                 batch_size=batch_size,
                                 callbacks=callbacks,
                                 verbose=verbose)
        self.history = history.history
        self.logger.info(
            "model training " + self.name + " for batch size " + str(batch_size) + " and epoch " + str(stopped_epoch))
        self.logger.info('[Model] Training Completed')
        timer.stop()
        return self.history

    def evaluate_model(self, xtest, ytest, batch_size):
        timer = Timer()
        timer.start()
        loss_and_metrics = self.model.evaluate(xtest, ytest, batch_size=batch_size)
        if len(loss_and_metrics) != 0:
            i = 0
            for metric in self.model.metrics_names:
                print(metric, '=', loss_and_metrics[i])
                i = i + 1
        self.logger.info(
            "model evaluation " + self.name + " for batch size " + str(batch_size))
        timer.stop()
        return loss_and_metrics

    def load_data(self, file_directory, file_data_var, file_sep=';', skiprows=0):
        self.logger.info("load file:" + file_directory + file_data_var)
        data_var = pd.read_csv(file_directory + file_data_var, sep=file_sep, skiprows=skiprows,
                               parse_dates=True)
        return data_var

    @staticmethod
    def display_data(data, columns_var):
        ax = plt.gca()
        data = data[columns_var]
        data.plot(ax=ax)
        ax.set_xlabel('Nbr of Days', fontsize=8)
        ax.set_ylabel(columns_var, fontsize=8)
        ax.set_title(columns_var + ' by days', fontsize=8)
        ax.tick_params(axis='both', which='major', labelsize=8)
        ax.legend(loc='upper left', frameon=False, fontsize='x-small')
        plt.tight_layout()
        plt.show()

    def convert_date_to_day(self, data_var):
        # todo sort by date doesnt work
        # data_var = data_var.sort_values(by='Date')
        # print(data_var)
        row_iterator = data_var.itertuples()
        day = 1
        days = []
        for _ in row_iterator:
            days.append(day)
            day = day + 1
        data_var['Days'] = days
        self.logger.debug('Add Days')
        data_var['Days'] = data_var['Days'].astype(int)
        data_var.set_index(['Days'], inplace=True)
        data_var.drop(columns=['Date'], inplace=True)
        return data_var

    def prediction(self, dataset, prediction_split=0.25, columns_features=None):
        timer = Timer()
        timer.start()
        features, labels, initial_data = self.get_train_data(dataset,
                                                             columns_features)
        prediction_size = int(self.train_data.size * (1 - prediction_split))
        data_var = initial_data.iloc[:prediction_size]
        window = data_var.values[-(self.window_size - 1):]
        data_to_predict = initial_data.iloc[prediction_size:]
        prediction = []

        for value in data_to_predict.values:
            coeff = float(window[0])
            window_to_predict = (window / coeff) - 1
            window_to_predict = window_to_predict.reshape(1, window_to_predict.shape[0], 1)
            prediction_value = self.model.predict(window_to_predict)
            prediction_value = np.reshape(prediction_value, prediction_value.size)
            prediction.append((prediction_value[0] + 1) * coeff)
            window = np.append(window, value)
            window = window[-(self.window_size - 1):]

        for i in range(len(self.train_data.index) - len(dataset.index) + 2):
            coeff = float(window[0])
            window_to_predict = (window / coeff) - 1
            window_to_predict = window_to_predict.reshape(1, window_to_predict.shape[0], 1)
            prediction_value = self.model.predict(window_to_predict)
            prediction_value = np.reshape(prediction_value, prediction_value.size)
            prediction.append((prediction_value[0] + 1) * coeff)
            window = np.append(window, (prediction_value[0] + 1) * coeff)
            window = window[-(self.window_size - 1):]

        index = np.arange(prediction_size, len(prediction) + prediction_size)
        predict = pd.DataFrame(data=prediction, index=index, columns=["Prediction_" + columns_features[0]])

        train_data = self.train_data.copy()
        train_data.rename(columns={train_data.columns[0]: "Train_" + columns_features[0]},
                          inplace=True)
        self.predict = [train_data, initial_data, predict]
        timer.stop()
        self.logger.info('predict LSTM ' + str(predict.size))
        return self.predict

    def display_predict(self):
        result = pd.concat(self.predict, axis=1, sort=False)
        result.plot()
        plt.title('Keras4layers_LSTM_CASA_Closing' + ' prediction')
        plt.show()

    def get_train_test_data(self, data_var, columns_features, test_split=1.0, method=None):
        timer = Timer()
        timer.start()
        features, labels, self.train_data = self.get_train_data(data_var,
                                                                columns_features)
        features, labels = LSTM_Model.normalize_windows(features, self.window_size)
        features, labels, ftest, ltest = self.get_test_data(method, test_split, features, labels)
        timer.stop()
        self.logger.info(
            'Get train and test data with method ' + method + ' value=' + str(
                test_split))
        return [features], labels, [ftest], ltest

    @staticmethod
    def get_train_data(dataset, columns_features):
        if columns_features is not None:
            train_features = dataset[columns_features].values
            columns_var = columns_features
        else:
            train_features = dataset.value
            columns_var = dataset.columns

        train_labels = dataset.index.values
        dataset = pd.DataFrame(data=train_features, index=train_labels, columns=columns_var)
        return train_features, train_labels, dataset

    @staticmethod
    def get_test_data(method, test_split, features, labels):
        ftest = None
        ltest = None
        if method == 'split':
            features, labels, ftest, ltest = LSTM_Model.get_split_sample(features, labels, test_split)
        elif method == 'random':
            features, labels, ftest, ltest = LSTM_Model.get_ramdom_sample(features, labels, test_split)
        return features, labels, ftest, ltest

    @staticmethod
    def get_split_sample(data_var_x, data_var_y, test_split):
        i_split = int(len(data_var_x) * test_split)
        return data_var_x[0:i_split], data_var_y[0:i_split], data_var_x[i_split:], data_var_y[i_split:]

    @staticmethod
    def get_ramdom_sample(data_var_x, data_var_y, test_split):
        i_split = int(len(data_var_x) * test_split)
        indices = np.sort(np.random.choice(len(data_var_x), i_split))
        xtest = []
        ytest = []
        for i in indices:
            xtest.append(data_var_x[i])
            ytest.append(data_var_y[i])
        return data_var_x, data_var_y, xtest, ytest

    @staticmethod
    def normalize_windows(data_feature, window_size):
        data_x = []
        data_y = []
        data_c = []
        for i in range(len(data_feature) - window_size):
            x, y, c = LSTM_Model.generate_samples(i, data_feature, window_size)
            data_x.append(x)
            data_y.append(y)
            data_c.append(c)
        data_x = np.array(data_x)
        data_y = np.array(data_y)
        data_x = data_x.reshape(data_x.shape[0], data_x.shape[1], 1)  # X.reshape(samples, timesteps, features)
        return data_x, data_y

    @staticmethod
    def generate_samples(i, y_var, window_size):
        window = y_var[i:i + window_size]
        # print('window=', window)
        normalised_window = []
        for p in window:
            normalised_value = ((float(p) / float(window[0])) - 1)
            normalised_window.append(normalised_value)
        x = normalised_window[:-1]
        y = normalised_window[-1]
        c = float(window[-1]) / float(y + 1)
        # print('generate window y=', y,' x=', x, 'coeff=',c,'true_y=',c*(y+1))
        return x, y, c

    def display_history(self, history_type=1):
        pp = pprint.PrettyPrinter(width=41, compact=True)

        if history_type == 1:
            pp.pprint(self.history)
        elif history_type == 2:
            for metric in self.model.metrics_names:
                plt.plot(self.history[metric])
                # plt.plot(self.history['val_' + metric])
                plt.title('Model ' + metric)
                plt.ylabel(metric)
                plt.xlabel('Epoch')
                plt.legend(['Train', 'Test'], loc='upper left')
                plt.show()

    def display_model(self, layers=False, bins=500):
        if layers:
            for layer in self.model.layers:
                weights = layer.get_weights()
                print('layer:', layer.name, ' with ', len(weights), ' weights and shape :')
                for weight in weights:
                    print(weight.shape)
                    plt.hist(np.ndarray.flatten(weight), bins=bins)
                    plt.show()
        netron.start('./' + self.name + '.h5')

    def save_model(self):
        timer = Timer()
        timer.start()
        self.model.save('./' + self.name + '.h5')
        with open('./' + self.name + '_train_data.pickle', 'wb') as f:
            pickle.dump(self.train_data, f, pickle.HIGHEST_PROTOCOL)
        with open('./' + self.name + '_train_history.pickle', 'wb') as f:
            pickle.dump(self.history, f, pickle.HIGHEST_PROTOCOL)
        timer.stop()

    def load_model(self):
        timer = Timer()
        timer.start()
        self.model = keras.models.load_model('./' + self.name + '.h5')
        with open('./' + self.name + '_train_data.pickle', 'rb') as f:
            self.train_data = pickle.load(f)
        with open('./' + self.name + '_train_history.pickle', 'rb') as f:
            self.history = pickle.load(f)
        timer.stop()
