import warnings

warnings.filterwarnings("ignore")
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)

import logging
logging.getLogger('tensorflow').disabled = True


from model import lstmmodel
