###### **LSTM Stock Market example**

A simple LSTM model for predicting stock market closing on a single values

A stock market file has been downloaded from https://www.abcbourse.com/download/historiques.aspx

For a given value you can download 2 years of cotation.
The CSV file is structured by ISIN;Date;Open;High;Low;Closing;Volume

The LSTM make only use of the closing value

main.py #Launch example <br>
1 - model training/save <br>
2 - model load/predict <br>
3 - display file closing value <br>
4 - display model network

util.py #For logging and time tracking utilities<br>
model/lstmmodel.py #the main class

This class provides tools to structure the input data and train/predict using LSTM Model<br>
The data should be structured on windows window_size = 5 so that input_timesteps of the model is 4 values (as features) to predict the fifth value <br>
The real fifth value of the window is then the label of the model<br>
The window size can be parameterized<br>
See the main file to see what can of functionnamlities are provided by the class 
